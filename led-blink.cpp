#include "mbed.h"

// The DigitalOut variables represent the 4 leds of the board
DigitalOut myled1(LED1);
DigitalOut myled2(LED2);
DigitalOut myled3(LED3);
DigitalOut myled4(LED4);

void myWait(int time)// Wait Time in ms
{
   Timer t;
   t.start();
   while(t.read_ms()<(time+1));    
   t.stop(); 
}


int main() {
    Timer t; // A timer object has been instantiated
    while(1) {
        myled1 = 1; //Led 1 is switched on
        myWait(1000); // Wait 1000ms
        myled1 = 0;// Led 1 is shut off after 1s
        myled2 = 1;// Led 2 is switched on
        myWait(1000); // Wait 1000ms
        myled2 = 0;  // Led 2 is shut off after 1s
        myled3 = 1;  // Led 3 is switched on
        myWait(1000); // Wait 1000ms
        myled3 = 0; // Led 3 is shut off after 1s
        myled4 = 1; // Led 4 is switched on
        myWait(1000); // Wait 1000ms
        myled4 = 0; // Led 4 is shut off
    }
}


